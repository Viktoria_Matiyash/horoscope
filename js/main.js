// fixed menu
$(window).bind("scroll", function () {
    $(window).scrollTop() > 225 ? $(".js-fixed-nav").addClass("header__nav--fixed") : $(".js-fixed-nav").removeClass("header__nav--fixed")
});

// adaptive menu
$(".adaptive-menu__open-menu").click(() => {
    $(".adaptive-menu__full-menu").slideDown()
});
$(".adaptive-menu__close-menu").click(() => {
    $(".adaptive-menu__full-menu").slideUp()
});

// active link in header
$(function () {
    $(".header__link").each(function () {
        window.location.href == this.href && $(this).addClass("active")
    })
});

// active tabs on main page
let gen = $('.general-horoscope__tabs');
gen.children('li').first().children('a').addClass('active')
    .next().addClass('is-open').show();
gen.on('click', 'li > a', function(event) {
    if (!$(this).hasClass('active')) {
        event.preventDefault();
        $('.general-horoscope__tabs .is-open').removeClass('is-open').hide();
        $(this).next().toggleClass('is-open').toggle();
        gen.find('.active').removeClass('active');
        $(this).addClass('active');
    } else {
        event.preventDefault();
    }
});

// slider news
$(".js-slider-news").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    infinite:true,
    speed:400,
    nav:true,
    autoplaySpeed:2000,
    margin: 10,
    responsive:[{
        breakpoint:1024,
        settings:{
            slidesToShow:3,
            slidesToScroll:1
        }},
        {
            breakpoint:600,
            settings:{
                slidesToShow:2,
                slidesToScroll:1
            }},
        {
            breakpoint:420,
            settings:{
                slidesToShow:1,
                slidesToScroll:1}}]
});

//tabs in zodiac sign
$(function() {
    let tab = $('.sign__tabs .sign__change > div');
    // tab.fadeOut(1000).filter(':first').fadeIn(1000);
    $('.sign__tabs .sign__categories .sign__list--item').click(function(){
        tab.hide();
        tab.filter(this.hash).fadeIn(1000);
        // $(this).toggleClass('active');
        $('.sign__tabs .sign__categories .sign__list--item').removeClass('active');
        $(this).addClass('active');
        return false;
    }).filter(':first').click();

    $('.tabs-target').click(function(){
        $('.sign__tabs .sign__categories .sign__list--item[href=' + $(this).data('id')+ ']').click();
    });
});

// change img
const sourceSwap = function () {
    let $this = $(this);
    let newSource = $this.data('hover-src');
    $this.data('hover-src', $this.attr('src'));
    $this.attr('src', newSource);
};

//active category in news
$(function () {
    $('.pagination__block img').hover(sourceSwap, sourceSwap);

    $(".news-page__category .js-sel").click(function (e) {
        e.preventDefault();
        $(".news-page__category .js-sel").removeClass("active");
        $(this).addClass("active");
    });
});

//fallout
$('.compatibility__dropdown').click(function () {
    $(this).attr('tabindex', 1).focus();
    $(this).toggleClass('active');
    $(this).find('.compatibility__dropdown-menu').slideToggle(300);
});
$('.compatibility__dropdown').focusout(function () {
    $(this).removeClass('active');
    $(this).find('.compatibility__dropdown-menu').slideUp(300);
});
$('.compatibility__dropdown .compatibility__dropdown-menu li').click(function () {
    $(this).parents('.compatibility__dropdown').find('.compatibility__women').text($(this).text());
    $(this).parents('.compatibility__dropdown').find('input').attr('value', $(this).data('horoscope'));
});

//conclusion result
$('.compatibility__content--left .compatibility__dropdown-menu li').click(function () {
    window.compatibility_left_value = $(this).parents('.compatibility__dropdown').find('input').val();
    document.getElementById("compatibility_result_link").href = "compatibility?a=" + window.compatibility_left_value + "&b=" + window.compatibility_right_value;
});
//conclusion result
$('.compatibility__content--right .compatibility__dropdown-menu li').click(function () {
    window.compatibility_right_value = $(this).parents('.compatibility__dropdown').find('input').val();
    document.getElementById("compatibility_result_link").href = "compatibility?a=" + window.compatibility_left_value + "&b=" + window.compatibility_right_value;
});

$(window).on('load', () => {
    let data = sessionStorage.getItem('signs_cached_svg');

    if (typeof data != "undefined" && data != null && data.length > 0) {
        $('.header__animation').html(data);
    } else {
        $.ajax({
            type: 'get',
            url: '/img/signs.svg',
            data: '',
            dataType: "html",
            success: function (data) {
                $('.header__animation').html(data);
                sessionStorage.setItem('signs_cached_svg', data);
            },
            error: function (error) {
//                alert('');
            }
        });
    }
});
